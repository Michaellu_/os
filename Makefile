.PHONY: all
all: build/boot.bin build/boot.img

build/boot.bin: boot/boot1.asm
	nasm -f bin -o $@ $<

build/boot.img: build/boot.bin
	dd if=/dev/zero of=$@ bs=512 count=2880
	dd if=$< of=$@ conv=notrunc

.PHONY: run
run:
	bochs -q -f bochsrc.txt

.PHONY: clean
clean:
	rm -r build/*
