org 0x7c00 # Shift below by 0x7c00 since BIOS loads to 0x7c00
bits 16 # Use 16 Bit instructions

start:
	jmp main

times 0x0B - $+start db 0 ; OEM Parameter Block zeroed out

;
; OEM Parameter Block
;
bpbBytesPerSector:  	dw 512
bpbSectorsPerCluster: 	db 1
bpbReservedSectors: 	dw 1
bpbNumberOfFATs: 	    db 2
bpbRootEntries: 	    dw 224
bpbTotalSectors: 	    dw 2880
bpbMedia: 	            db 0xF0
bpbSectorsPerFAT: 	    dw 9
bpbSectorsPerTrack: 	dw 18
bpbHeadsPerCylinder: 	dw 2
bpbHiddenSectors: 	    dd 0
bpbTotalSectorsBig:     dd 0
bsDriveNumber: 	        db 0
bsUnused: 	            db 0
bsExtBootSignature: 	db 0x29
bsSerialNumber:	        dd 0xa0a1a2a3
bsVolumeLabel: 	        db "MIKE OS    "
bsFileSystem: 	        db "FAT12   "

msg db "Hello World!", 0

print:
	lodsb ; Load *DS:SI into AL, inc SI if DF=0 else dec SI if DF=1

	cmp al, 0 ; Exit if \0 reached
	je print_end

	mov ah, 0x0e ; Print character
	int 0x10

	jmp print
print_end:
	ret

main:
	xor ax, ax ; Null Segment registers
	mov ds, ax ; We already have 0x7c00
	mov es, ax ; offset.

	mov si, msg ; Print Hello World!
	call print

	cli ; Clear Interrupts
	hlt ; Halt the system

times 510 - ($-$$) db 0 ; Make sure we have 512 byte. Necessary
dw 0xaa55 ; Boot Signature
